import numpy as np
from sklearn.ensemble import RandomForestClassifier

from read_preprocessed_dataset import read_dataset


def write_result(y_pred):
    with open("data/prediction.txt", "w") as f:
        np.savetxt(f, y_pred)


def fit_predict(X_train, y_train, X_test, random_state=777):
    clf = RandomForestClassifier(random_state=random_state)
    clf.fit(X_train, y_train)

    return clf.predict(X_test)


if __name__ == '__main__':
    X_train, y_train, X_test, y_test = read_dataset("data/train.npy", "data/test.npy")
    y_pred = fit_predict(X_train, y_train, X_test)
    write_result(y_pred)
