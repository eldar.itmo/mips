import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split


def fetch(path, test_size=0.3, random_state=777):
    df = pd.read_csv(path, header=0, names=['sepal length', 'sepal width', 'petal length', 'petal width', 'class'])

    df = df.replace("Iris-setosa", 0)
    df = df.replace("Iris-versicolor", 1)
    df = df.replace("Iris-virginica", 2)

    return train_test_split(
        df[df.columns[:-1]],
        df[df.columns[-1:]],
        test_size=test_size,
        random_state=random_state
    )


def save_as_npy(path, *data):
    # concat arrays
    stacked_data = np.hstack([l.to_numpy() for l in data])
    # save as np binary file
    with open(path, "wb") as f:
        np.save(f, stacked_data)


def main():
    X_train, X_test, y_train, y_test = fetch("data/iris.data")
    save_as_npy("data/train.npy", X_train, y_train)
    save_as_npy("data/test.npy", X_test, y_test)


if __name__ == "__main__":
    main()
