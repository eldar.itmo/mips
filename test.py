from sklearn.metrics import accuracy_score

from read_preprocessed_dataset import read_dataset
from random_forest import fit_predict


def test():
    X_train, y_train, X_test, y_test = read_dataset("data/train.npy", "data/test.npy")
    y_pred = fit_predict(X_train, y_train, X_test)

    accuracy = accuracy_score(y_test, y_pred)

    print(f"Test info accuracy is: {accuracy}")

    assert accuracy > 0


if __name__ == '__main__':
    test()
