import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

from read_preprocessed_dataset import read_dataset


def read_test_dataset():
    X_train, y_train, X_test, y_test = read_dataset("data/train.npy", "data/test.npy")
    return y_test


def read_pred_dataset(path="data/prediction.txt"):
    return np.genfromtxt(path)


def write_results(accuracy, f1, filename="scoring.txt"):
    with open(f"data/{filename}", "w") as f:
        print("Accuracy:", accuracy, file=f)
        print("F1 score:", f1, file=f)


def make_scoring():
    y_test = read_test_dataset()
    y_pred = read_pred_dataset()
    accuracy = accuracy_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred, average="macro")
    write_results(accuracy, f1)


if __name__ == '__main__':
    make_scoring()
