import numpy as np


def read_dataset(train_path, test_path):
    with open(train_path, "rb") as f:
        train_ds = np.load(f)

    with open(test_path, "rb") as f:
        test_ds = np.load(f)

    X_train, y_train = train_ds[:, :-1], train_ds[:, -1]
    X_test, y_test = test_ds[:, : -1], test_ds[:, -1]

    return X_train, y_train, X_test, y_test
